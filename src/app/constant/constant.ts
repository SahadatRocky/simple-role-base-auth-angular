
export const Constant = {

    menus : [
      {
        path : 'client',
        text : 'Client',
        roles : ['Admin']
      },
      {
        path : 'user',
        text : 'User',
        roles : ['Admin', 'ClientAdmin']
      },
      {
        path : 'package',
        text : 'Package',
        roles : ['Admin']
      },
      {
        path : 'client-package',
        text : 'Cliet-Package',
        roles : ['Admin']
      },
      {
        path : 'dashboard',
        text : 'Dashboard',
        roles : ['Admin', 'ClientAdmin']
      },
      {
        path : 'room',
        text : 'Room',
        roles : ['ClientAdmin']
      },
      {
        path : 'booking',
        text : 'booking',
        roles : ['ClientAdmin']
      },
    ]

}
