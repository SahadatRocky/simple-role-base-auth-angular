import { Component } from '@angular/core';
import { Constant } from '../constant/constant';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrl: './admin-layout.component.css'
})
export class AdminLayoutComponent {

  menus : any[] = [];
  filteredMenu : any[] = [];
  role : string = '';
  constructor(){
    this.menus = Constant.menus;
    const userData = localStorage.getItem('userData');
    if(userData){
      let perseObj = JSON.parse(userData);
      this.role = perseObj.role;
    }

    this.menus.forEach((el:any) =>{
      const isMenuPresent = el.roles.find((role: any)=> role == this.role);
      if(isMenuPresent){
         this.filteredMenu.push(el);
      }
    })
  }

}
