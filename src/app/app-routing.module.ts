import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { ClientComponent } from './client/client.component';
import { UserComponent } from './user/user.component';
import { PackageComponent } from './package/package.component';
import { ClientPackageComponent } from './client-package/client-package.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RoomComponent } from './room/room.component';
import { RoomBookingComponent } from './room-booking/room-booking.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path : '',
    component : AdminLayoutComponent,
    children:[
      {
        path : 'client',
        component : ClientComponent,  //admin
      },
      {
        path : 'user',
        component : UserComponent,  //admin + client
      },
      {
        path : 'package',
        component : PackageComponent,  //admin
      },
      {
        path : 'client-package',
        component : ClientPackageComponent, //admin
      },
      {
        path : 'dashboard',
        component : DashboardComponent, // admin + client
      },
      {
        path : 'room',
        component : RoomComponent, //client
      },
      {
        path : 'booking',
        component : RoomBookingComponent, //client
      },

    ]

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
