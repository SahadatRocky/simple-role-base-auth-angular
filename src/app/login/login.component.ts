import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{


  loginObj: any = {
    userName:'',
    userPassword:''
  };

  isRegister: boolean = false;
  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit(): void {
  }

  onLogin(){
    debugger;
    this.http.get("http://localhost:3000/login")
    .subscribe((response: any)=>{
      debugger;
      if(response) {
       const objFound = response.find((el : any) => el.userName ===  this.loginObj.userName  && el.userPassword === this.loginObj.userPassword);
       console.log('objFound::',objFound);
       if(objFound){
          localStorage.setItem('userData',JSON.stringify(objFound));
          this.router.navigateByUrl('dashboard');
       }
      } else {
        alert("Login Failed")
      }
    })
  }
}
