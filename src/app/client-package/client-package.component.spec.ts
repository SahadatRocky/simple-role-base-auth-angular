import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientPackageComponent } from './client-package.component';

describe('ClientPackageComponent', () => {
  let component: ClientPackageComponent;
  let fixture: ComponentFixture<ClientPackageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ClientPackageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ClientPackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
